###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from unittest.mock import patch

from lb.nightly.configuration import DBASE, Package, Project, Slot
from pytest import raises

from lb.nightly.scheduler._types import DeploymentTarget

patch_conf = patch(
    "lb.nightly.scheduler._types.service_config",
)


@patch_conf
@patch("requests.put")
def test_trigger_deployment(mock_req, mock_conf, tmp_path):
    project = Project("MyProject", "HEAD")

    conf = {
        "artifacts": {
            "uri": "http://user:pass@my-artifacts.repo",
        },
    }
    mock_conf.return_value = conf
    target = DeploymentTarget(project, stage="checkout")

    with raises(RuntimeError):
        target.trigger_deployment()

    conf["lbtask"] = {"hook": "http://my-hook/install/nightlies", "token": "asdf"}
    mock_conf.return_value = conf
    target = DeploymentTarget(project, stage="checkout")

    target.trigger_deployment()
    mock_req.assert_called_with(
        "http://my-hook/install/nightlies/MyProject/HEAD/",
        params={
            "url": "http://my-artifacts.repo/checkout/MyProject/2d/2df4f6746f6bc50e61c81b787bce2922ccbdee28bb52c10ae0489080575dd2fe.zip"
        },
        headers={"Authorization": "Bearer asdf"},
    )
    target = DeploymentTarget(
        project, stage="build", platform="x86_64_v2-centos7-gcc11-opt"
    )
    # artifact_name has to be specified for triggering binaries
    with raises(ValueError):
        target.trigger_deployment()
    target.trigger_deployment("artifact_name.zip")
    mock_req.assert_called_with(
        "http://my-hook/install/nightlies/MyProject/HEAD/InstallArea/x86_64_v2-centos7-gcc11-opt/",
        params={"url": "http://my-artifacts.repo/artifact_name.zip"},
        headers={"Authorization": "Bearer asdf"},
    )
    slot = Slot("test-my-slot", projects=[project])
    target = DeploymentTarget(slot, stage="deployment_dir")
    target.trigger_deployment()
    mock_req.assert_called_with(
        "http://my-hook/install/nightlies/testing/test-my-slot/0/",
        params={
            "url": "http://my-artifacts.repo/testing/test-my-slot/0/deployment_dir.zip"
        },
        headers={"Authorization": "Bearer asdf"},
    )

    data_project = DBASE(packages=[Package("Group/APackage", "v1r0")])
    target = DeploymentTarget(data_project.packages[0], stage="checkout")
    target.trigger_deployment()
    mock_req.assert_called_with(
        "http://my-hook/install/nightlies/DBASE/Group/APackage/",
        params={
            "url": "http://my-artifacts.repo/checkout/Group/APackage/2d/2d3e8058ebe13fb64fa78ccf9f253284a6c651ef7fc613a242f302ffc643d266.zip"
        },
        headers={"Authorization": "Bearer asdf"},
    )
    target = DeploymentTarget(data_project, stage="checkout")
    # data projects are not allowed
    with raises(ValueError):
        target.trigger_deployment()
