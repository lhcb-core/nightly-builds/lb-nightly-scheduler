###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys
from datetime import timedelta
from unittest.mock import ANY, MagicMock, call, patch

import luigi
from pytest import raises

from lb.nightly.scheduler._types import ArtifactTarget
from lb.nightly.scheduler.workflow import Checkout


@patch(
    "lb.nightly.db.database.Database.docname",
)
@patch(
    "lb.nightly.rpc.checkout",
)
@patch(
    "lb.nightly.scheduler._types.DeploymentTarget.deployment_ready",
)
@patch(
    "lb.nightly.scheduler._types.MAX_TIME_WAITING_FOR_DEPLOYMENT", timedelta(seconds=3)
)
@patch(
    "lb.nightly.scheduler._types.TIME_BETWEEN_DEPLOYMENT_CHECKS", timedelta(seconds=1)
)
@patch(
    "lb.nightly.db.connect",
)
@patch("lb.nightly.scheduler._types.DeploymentTarget.trigger_deployment")
@patch(
    "lb.nightly.scheduler._types.service_config",
    **{
        "return_value": {
            "artifacts": {
                "uri": "http://user:pass@/some/path",
            }
        }
    },
)
def test_checkout_task(
    mock_conf,
    mock_cache_install,
    mock_db,
    mock_cache,
    mock_check,
    mock_doc,
    caplog,
):
    from lb.nightly.configuration import Project

    from lb.nightly.scheduler.workflow import Checkout

    project = Project("Gaudi", "master")
    create_sources = Checkout(project=project)

    mock_target = ArtifactTarget(project=project, stage="checkout")
    mock_target.exists = MagicMock(return_value=True)
    create_sources.output = MagicMock(return_value=mock_target)

    luigi.build([Checkout(project=project)], local_scheduler=True)
    mock_check.assert_not_called()

    mock_target.exists = MagicMock(return_value=False)
    create_sources.output = MagicMock(return_value=mock_target)
    luigi.build([Checkout(project=project)], local_scheduler=True)
    mock_check.assert_called_once()


@patch(
    "lb.nightly.db.database.Database.docname",
)
@patch(
    "lb.nightly.rpc.checkout",
)
@patch(
    "lb.nightly.scheduler._types.DeploymentTarget.deployment_ready",
)
@patch(
    "lb.nightly.scheduler._types.MAX_TIME_WAITING_FOR_DEPLOYMENT", timedelta(seconds=3)
)
@patch(
    "lb.nightly.scheduler._types.TIME_BETWEEN_DEPLOYMENT_CHECKS", timedelta(seconds=1)
)
@patch(
    "lb.nightly.db.connect",
)
@patch("lb.nightly.scheduler._types.DeploymentTarget.trigger_deployment")
@patch(
    "lb.nightly.scheduler._types.service_config",
    **{
        "return_value": {
            "artifacts": {
                "uri": "http://user:pass@/some/path",
            }
        }
    },
)
def test_deploy_sources_task(
    mock_conf,
    mock_cache_install,
    mock_db,
    mock_cache,
    mock_check,
    mock_doc,
    caplog,
):
    from lb.nightly.configuration import Project

    from lb.nightly.scheduler.workflow import DeploySources

    project = Project("Gaudi", "master")
    deploy_sources = DeploySources(project=project)

    mock_cache.return_value = False

    mock_cache.reset_mock()
    deploy_sources.run()
    mock_cache.assert_has_calls([ANY] * 3)
    assert not deploy_sources.output().exists()
    assert "ERROR" in caplog.text
    assert (
        "Giving up after waiting for deployment of Gaudi/master  checkout artifact for 0:00:03"
        in caplog.text
    )

    mock_cache.reset_mock()
    mock_cache.side_effect = [False, False, False, True, True]
    deploy_sources.run()
    mock_cache.assert_has_calls([ANY] * 3)
    assert deploy_sources.output().exists()
